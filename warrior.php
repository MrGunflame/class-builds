<?php

$builds = array (
  array(
    'class' => "spellbreaker",
    'buildname' => "Power Spellbreaker",
    'armor' => array (
      "74929",
      "76551",
      "76245",
      "75858",
      "74962",
      "70593"
    ),
    'armorname' => array (
      "Marauder's",
      "Marauder's",
      "Marauder's",
      "Marauder's",
      "Marauder's",
      "Marauder's"
    ),
    'rune' => "24714",
    'runeoptional' => array (
    ),
    'infusion' => '43254',
    'weapon1' => array (
      "46763",
      "-1",
      "84505",
      "24607"
    ),
    'weapon2' => array (
      "46774",
      "46759",
      "24615",
      "24575"
    ),
    'trinket' => array (
      "79980",
      "80002",
      "80002",
      "81467",
      "80793",
      "80793"
    ),
    'trinket_stat' => array (
      "161",
      "161",
      "161",
      "161",
      "161",
      "161"
    ),
    'trinketname' => array (
      "Berserker",
      "Berserker",
      "Berserker",
      "Berserker",
      "Berserker",
      "Berserker"
    ),
    'food' => array (
      "43360",
      "9443"
    ),
    'skill' => array (
      "21815",
      "14392",
      "43123",
      "14412",
      "45333"
    ),
    'skilloptional' => array (
    ),
    'traits1' => array (
      "defense",
      "2",
      "1",
      "1"
    ),
    'traits2' => array (
      "discipline",
      "2",
      "3",
      "1"
    ),
    'traits3' => array (
      "spellbreaker",
      "1",
      "1",
      "1"
    ),
    'description' => "The Power Spellbreaker offers a lot of Boonstrip, primarly thanks to Winds of Disenchantment. It also provides a lot of CC with hammer skills as well as decent Damage.",
    'roles' => array (
      "Boonsstrip",
      "CC",
      "Damage"
    ),
    'guide' => "
	Coming Soon
    ",
  ),
);

?>
