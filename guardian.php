<?php

$builds = array (
  array(
    'class' => "firebrand",
    'buildname' => "Support Firebrand",
    'armor' => array (
      "72698",
      "73009",
      "75142",
      "70794",
      "77022",
      "71456"
    ),
    'armorname' => array (
      "Minstrel's",
      "Minstrel's",
      "Minstrel's",
      "Minstrel's",
      "Minstrel's",
      "Minstrel's"
    ),
    'rune' => "24842",
    'runeoptional' => array (
    ),
    'infusion' => '43250',
    'weapon1' => array (
      "75200",
      "",
      "74326",
      "24582"
    ),
    'weapon2' => array (
      "71457",
      "74748",
      "74326",
      "24607"
    ),
    'trinket' => array (
      "79980",
      "80002",
      "80002",
      "81467",
      "80793",
      "80793"
    ),
    'trinket_stat' => array (
      "1134",
      "1134",
      "1134",
      "1134",
      "1134",
      "1134"
    ),
    'trinketname' => array (
      "Minstrel",
      "Minstrel",
      "Minstrel",
      "Minstrel",
      "Minstrel",
      "Minstrel"
    ),
    'food' => array (
      "68634",
      "67528"
    ),
    'skill' => array (
      "41714",
      "9153",
      "9246",
      "45460",
      "43357"
    ),
    'skilloptional' => array (
    ),
    'traits1' => array (
      "honor",
      "1",
      "2",
      "3"
    ),
    'traits2' => array (
      "virtues",
      "2",
      "2",
      "2"
    ),
    'traits3' => array (
      "firebrand",
      "3",
      "3",
      "3"
    ),
    'description' => "As a firebrand you are the main source of Support for your Subgroup so always stay with your tag and look after your allies. Each party should have at least one Firebrand.",
    'roles' => array (
      "Healing",
      "Condi Cleanse",
      "Boons"
    ),
    'guide' => '
    With this build you will be the main source of healing and boons for your group.<br>
<h3>TOMES</h3>
 Starting with each tome usage you should be using your <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="44364"></span>, <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="41258"></span> for pulls if you are doing a surprise push with or without invisibility and <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="42898"></span> for extra burning with the buff on your allies.<br>
In a normal fight use your <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="42259"></span>, <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="44455"></span> specially for retaliation buff then <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="40988"></span> for resistance buff for you and your group drop your <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="41836"></span> while your group and the enemy are auto casting on each other for reflection also use it on siege so enemy cant disable them or auto cast on you, also <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="42986"></span> in fight if u think your party needs stability.<br>
About <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="41780"></span> when u see your party is going low on health and need healing use it <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="42925"></span> for extra healing buff, then <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="42008"></span> water field and call it in TS or Discord so your group can blast it for more healing, also <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="45128"></span> use it once for vigor buff. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="40679"></span> is good cleanse spam it for cleanse but <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="45022"></span> when u see your group need healing superfast and its emergency, u can just spam <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="45022"></span> and bomb heal.<br>
That\'s with tomes.
<h3>UTILITIES</h3>
 About the utilities do not forget that <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="41714"></span> buffs you and allies with aegis, that will heal you and allies when it’s popped on enemy strike, so time your eagis buffs for example revenants phase smash.<br>
 <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="45460"></span> a nice condition cleanse nothing special about it use it when u need a fast condition cleanse u can use it because no cast time. (Make sure you clean any cripple, chill as fast as possible).<br>
 <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9153"></span> is your big stability use it big push and make sure your party always have stability buff and remember it says it’s a stun break but it only breaks your stun not your party.<br>
 <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="43357"></span> is a real good stun break for u and allies standing in front of you.<br>
 <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9246"></span> or MI in commander calls is normally used to resurrect a downed ally use it as fast as possible when you see ally going down near the group you can also use it to shadow step to an ally but when your blob is being hammered by an enemy surprise push and you don\'t have time to use healing tome, use your MI superfast on allies and heal them.<br>
That’s about utilities.
<h3>STAFF</h3>
 About the staff skills start with <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9144"></span> commanders call it as LINE so drop it between your and enemy blob it will stop revenants phase if they have no stability also enemy blob can’t just pass through it so u can also use it to block a choke make sure you drop it on commander’s call.
Staff <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9265"></span> is what commanders call MIGHT or EMPOWER will buff u and allies with 12 stacks of might make sure you will use it on commander call it also heals allies for a small amount for last resort healing.<br>
Staff <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9143"></span> is your symbol for swiftness buff and <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9140"></span> is a good blast with short cool down use it on commander because it also does a good amount of healing.
<h3>MACE/SHIELD</h3>
For harder fights being used instead of hammer and great sword your <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9108"></span> will heal your allies not anything big but anyway <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9086"></span> blocks next incoming attack and heal you and your allies also buffs you with eagis that will block another attack and heal you again and <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9087"></span> on shield will buff you and allies in front with eagis and protection buffs and <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9091"></span> is a good dome that blocks projectiles and pushes enemy away so yes it’s a CC.
<h3>NOTES</h3>
<ol>
<li>Your tomes auto attack affects you and allies in front of you.</li>
<li>Mantras affects you and allies in front of you.</li>
<li>Stand your ground won’t break your allies stun.</li>
<li>Staff <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9140"></span> is your blast.</li>
<li>You can combo staff <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9143"></span> or <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9144"></span> then <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="9140"></span> for condition cleanse.</li>
<li>Cleanse chill, cripple, poison ,slow as fast as possible.</li>
</ol>
    ',
  ),
);

?>
