<?php

$builds = array (
  array(
    'class' => "scrapper",
    'buildname' => "Support Scrapper",
    'armor' => array (
      "75022",
      "76658",
      "70834",
      "76729",
      "75340",
      "77204"
    ),
    'armorname' => array (
      "Minstrel's",
      "Minstrel's",
      "Minstrel's",
      "Minstrel's",
      "Minstrel's",
      "Minstrel's"
    ),
    'rune' => "24842",
    'runeoptional' => array (
      "48907"
    ),
    'infusion' => '43250',
    'weapon1' => array (
      "76262",
      "-1",
      "74326",
      "24582"
    ),
    'weapon2' => array (
      "-1",
      "-1",
      "-1",
      "-1"
    ),
    'trinket' => array (
      "79980",
      "80002",
      "80002",
      "81467",
      "80793",
      "80793"
    ),
    'trinket_stat' => array (
      "1134",
      "1134",
      "1134",
      "1134",
      "1134",
      "1134"
    ),
    'trinketname' => array (
      "Minstrel",
      "Minstrel",
      "Minstrel",
      "Minstrel",
      "Minstrel",
      "Minstrel"
    ),
    'food' => array (
      "68634",
      "67528"
    ),
    'skill' => array (
      "5802",
      "5933",
      "30101",
      "29739",
      "30815"
    ),
    'skilloptional' => array (
    ),
    'traits1' => array (
      "inventions",
      "1",
      "2",
      "3"
    ),
    'traits2' => array (
      "alchemy",
      "3",
      "1",
      "2"
    ),
    'traits3' => array (
      "scrapper",
      "1",
      "1",
      "1"
    ),
    'description' => "Support Scrappers can provide lots of healing and Condition Cleanse for their Subgroup. Due to one Trait it will convert Conditions instead of cleansing them. It also comes with other useful Support like Damage Reduction, Stealth and a great Superspeed uptime. One Scrapper each 10 Squad Members is the perfect balance.",
    'roles' => array(
      "Healing",
      "Condi Cleanse",
      "Superspeed",
      "Stealth"
    ),
    'guide' => '
    This Build provides an extreme amount of condition cleanses. Due to <span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="1871"></span> each Condition cleansed will be converted to a Boon and with <span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="520"></span> you can do a ton of healing. Your main Job during fights is to camp your <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5802"></span> and use your Cleansing Skills. Additionally you will be able to stealth up to 10 Players using <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30815"></span> and reduce allied incoming damage with <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30101"></span>. Passively you will provide great Superspeed uptime to your Subgroup.
<h4>Build Variants</h4>
Runes:
<ul>
<li><span data-armory-embed="items" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="24842"></span>: additional healing and boon duration</li>
<li><span data-armory-embed="items" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="48907"></span>: Doubles the amount of cleanses</li>
</ul>
Inventions:
<ul>
<li><span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="394"></span>: +7% Damage Reduction with Protection</li>
<li><span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="1901"></span>: Extra <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29772"></span> on low health</li>
</ul>
<ul>
<li><span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="1834"></span>: Additional Healing on blast</li>
<li><span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="445"></span>: Extra Movement Speed</li>
</ul>
Scrapper:
<ul>
<li><span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="1981"></span>: More Self Sustain</li>
<li><span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="1849"></span>: High Quickness uptime, more heals</li>
</ul>
<h4>Healing</h4>
Your source of healing is your <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5802"></span>.
Its <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30521"></span> provides a constant high amount of healing. Whenever you have no other skills to cast, use <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30521"></span>. It will provide constant healing to your group. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="49082"></span> and <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29547"></span> provide a good amount of burst heal. Prioritize <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29547"></span> since it has a lower cooldown. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="49045"></span> can be blasted with <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="50444"></span> for extra healing. For an additional blast use <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5936"></span> from <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5933"></span>.
</h4>
<h4>Cleansing</h4>
One of your main roles is to keep your group clean from conditions. Due to <span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="1871"></span> each cleansed condition will get converted into a boon instead. That way you will provide a ton of boons just by cleansing your friends. <span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="520"></span> will additionally apply Regeneration for each Condition cleansed.
The build ships 17 cleanses with a maximum Cooldown of 20 Seconds. Since all of those are pulsing you can take <span data-armory-embed="items" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="48907"></span> to double the amount of cleanses. If you are still lacking cleanses, you can blast Light Fields for 1 additional cleanse (2 with Antitoxin).
<br>Most important Cleansing Skills:
<ul>
<li><span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="49045"></span></li>
<li><span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5965"></span></li>
<li><span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5937"></span></li>
<li><span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29739"></span></li>
</ul>
<br>
When to use which cleansing skill?
<br>
<span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5965"></span> only cleanses from allies, not from yourself. You want to use this Skill when you have no conditions, but your group has. If you also have conditions yourself, your first Skill to be used should be <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29739"></span>, as you can also use <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="49045"></span> as additional healing through blasting the water Field. After that use <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="49045"></span>.
For even more cleanses switch back to your <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5933"></span> and drop <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5937"></span> and use <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5936"></span> to blast it. Do not use all your cleanses at the same time or you will be without any for the next 15 seconds.
<h4>Combos</h4>
Effectively using Combos on this Build can make it even more efficient. Keep the following in Mind:
When playing with <span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="1834"></span> each time you create a combo using a blast finisher you will provide an additional strong AoE heal.
Due to <span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="2014"></span> each time you leap in any Combo Field, you will grant yourself Superspeed. When using an blast finisher, you will grant Superspeed to your group.
<br>Most important Combos:<br>
<ul>
<li>Water + Blast <span class="icon-arrow-right2"></span> AoE Heal</li>
<li>Water + Leap <span class="icon-arrow-right2"></span> Personal Heal</li>
<li>Smoke + Blast <span class="icon-arrow-right2"></span> AoE Stealth</li>
<li>Smoke + Leap <span class="icon-arrow-right2"></span> Personal Stealth</li>
<li>Light + Blast <span class="icon-arrow-right2"></span> AoE Cleanse (1 Condition, 2 with <span data-armory-embed="items" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="48907"></span>)</li>
<li>Lightning + Blast <span class="icon-arrow-right2"></span> AoE Swiftness</li>
</ul>
<h4>Skill Overview</h4>
<h5>Kits:</h5>
For anyone unfamiliar with Engineer Kits. Kits are Skills that will replace your Weapon Skills when activated, just like Weapon Swap on other Classes. You can activate and deactivate Kits at any time without cooldowns. In this Build two Kits are used: <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5802"></span> and <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5933"></span>.
<h5>Hammer:</h5>
You shouldn\'t be using your hammer, unless your group needs no support (healing, cleanses). You can use the Autoattack and <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30088"></span> to deal decent melee damage. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30665"></span> will leap towards your foes three times. It may be used to keep up with your group but be used carefully in fights. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30713"></span> is the most important skill to keep on cooldown. It can stun up to 5 foes within an AoE. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29840"></span>  will block 2 seconds. It can be used together will <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30101"></span> to negate the redirected damage.
<h5>Med Kit:</h5>
As already stated above, the <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5802"></span> is your main source of healing and is where you want to stay the most time of the fight. The <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30521"></span> autoattack will heal allies in front of you but not yourself. Same counts for <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29547"></span>. It will shoot 5 Bandages in your front, healing up to 5 allies. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="49082"></span> is a simple and strong AoE heal, also healing yourself. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="49045"></span> will cleanse 5 conditions and is a water Field for 4 Seconds. You may blast it for more healing. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="50444"></span> will throw a Bomb that will apply Swiftness, Vigor and Regeneration after 1 Second. It will also function as a blast finisher.
<h5>Elixir Gun:</h5>
The most important Skills to keep in Mind when it comes to your <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5933"></span> are <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5965"></span>, <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5936"></span> and <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5937"></span>. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5965"></span> will work similar to <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30521"></span>, cleansing 5 conditions from allies and poisoning enemies. Again this doesn‘t apply to yourself. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5936"></span> is a great blast finisher dropping an AoE that also does decent unblockable damage. When casting it, you will be kicked backwards, however you can use Weapon Swap in mid air to cancel the leap. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5937"></span> is a great AoE to regenerate your group. It will stay for 10 Seconds and cleanse 1 condition on impact.
<h4>Utilities:</h4>
As utilities, we only use Gyros. Gyros are AoE that will follow with Character. The <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30101"></span> will absorb some damage from allies and redirect it to yourself. You will get Barrier each second. It\'s basicaly to be used on cooldown in combat or when engaging. It will also function as an Lightning Field. The <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29739"></span> will cleanse 1 conditon from allies each second for 5 seconds total and is a Light Field. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30815"></span> is a really strong ability. When used it will stealth 5 allies each second. It also drops a Smoke Field. For more info on how to stealth check the guide below.
<h4>Toolbelt (F1-F5):</h4>
<span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29772"></span> is a good personal heal, but it will also heal allies due to <span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="1916"></span>. Since its casting time is high, make sure your group won‘t die while you cast <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29772"></span>. Always focus on your allies rather than yourself. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5966"></span> will apply some Regeneration to allies, but most importantly is your panic stunbreak. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30027"></span> will create a Dome around yourself. It will block incoming Projectiles and grant yourself long lasting Stability. Simply use it on cooldown in combat. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30279"></span> will place an AoE poisoning foes. Not very usefull, but can be used to slow enemy resses or as a throwaway Combo Field to blast for Superspeed. <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="56920"></span> will place a Lightning Field and provides Superspeed on impact.
<h4>Superspeed</h4>
With this Build you can apply a high Superspeed uptime. Since you‘re running with <span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="1917"></span>, Gyros will passively apply Superspeed when they end. Make sure you don‘t cast all Gyros at the same time, so you get the full time of Superspeed. For instant Superspeed simply cast <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="56920"></span> on your feet. If you still need more, cast a blast finisher(<span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="5936"></span>, <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="50444"></span>) in any Combo Field.
<h4>Stealthing</h4>
With some help of your allies you can easily stealth up to 10 people for about 15 seconds. This is very efficient when your foes don‘t see you coming and are not prepared to get bombed. To stealth 5 allies simply use <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30815"></span>. If you want to stealth more allies, drop your <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="30815"></span> and let others (and yourself) blast it.
    ',
  )
);

?>
