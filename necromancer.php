<?php

$builds = array (
  array(
    'class' => "scourge",
    'buildname' => "Power Scourge",
    'armor' => array (
      "74929",
      "76551",
      "76245",
      "75858",
      "74962",
      "70593"
    ),
    'armorname' => array (
      "Berserker's",
      "Berserker's",
      "Berserker's",
      "Berserker's",
      "Berserker's",
      "Berserker's"
    ),
    'rune' => "24836",
    'runeoptional' => array (
    ),
    'infusion' => '43254',
    'weapon1' => array (
      "46769",
      "46760",
      "24615",
      "24607"
    ),
    'weapon2' => array (
      "46773",
      "-1",
      "24615",
      "24575"
    ),
    'trinket' => array (
      "79980",
      "80002",
      "80002",
      "81467",
      "80793",
      "80793"
    ),
    'trinket_stat' => array (
      "161",
      "583",
      "583",
      "583",
      "583",
      "161"
    ),
    'trinketname' => array (
      "Berserker",
      "Berserker",
      "Berserker",
      "Berserker",
      "Berserker",
      "Berserker"
    ),
    'food' => array (
      "43360",
      "9443"
    ),
    'skill' => array (
      "43148",
      "10546",
      "10545",
      "46760",
      "42355"
    ),
    'skilloptional' => array (
    ),
    'traits1' => array (
      "curses",
      "2",
      "2",
      "3"
    ),
    'traits2' => array (
      "soul reaping",
      "2",
      "1",
      "2"
    ),
    'traits3' => array (
      "scourge",
      "1",
      "2",
      "1"
    ),
    'description' => "",
    'roles' => array (
      "Damage",
      "Boonstrip",
      "Barrier"
    ),
    'guide' => "
    "
  )
);

?>
