<?php

$builds = array (
  array(
    'class' => "herald",
    'buildname' => "Hammer Herald",
    'armor' => array (
      "74929",
      "76551",
      "76245",
      "75858",
      "74962",
      "70593"
    ),
    'armorname' => array (
      "Marauder's",
      "Marauder's",
      "Marauder's",
      "Marauder's",
      "Marauder's",
      "Marauder's"
    ),
    'rune' => "24836",
    'runeoptional' => array (
    ),
    'infusion' => '43254',
    'weapon1' => array (
      "46763",
      "-1",
      "24615",
      "24868"
    ),
    'weapon2' => array (
      "46773",
      "-1",
      "24607",
      "24575"
    ),
    'trinket' => array (
      "79980",
      "80002",
      "80002",
      "81467",
      "80793",
      "80793"
    ),
    'trinket_stat' => array (
      "161",
      "161",
      "161",
      "161",
      "161",
      "161"
    ),
    'trinketname' => array (
      "Berserker",
      "Berserker",
      "Berserker",
      "Berserker",
      "Berserker",
      "Berserker"
    ),
    'food' => array (
      "43360",
      "9443"
    ),
    'skill' => array (
      "28085",
      "26650"
    ),
    'skilloptional' => array (
    ),
    'traits1' => array (
      "devastation",
      "2",
      "3",
      "1"
    ),
    'traits2' => array (
      "invocation",
      "2",
      "1",
      "3"
    ),
    'traits3' => array (
      "herald",
      "2",
      "1",
      "3"
    ),
    'description' => "In this build try to stick with your commander and do as much ranged damage as you can also bit of support.",
    'roles' => array (
      "Range Spike",
      "Melee Cleave",
      "CC",
      "Superspeed",
      "Damage Mitigation"
    ),
    'guide' => '
    With this build running you are doing the blob’s range spike also bit of support.
    <h4>DRAGON STANCE</h4>
    <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="28379"></span>: make sure u always got it activated for permanent fury boon for you and allies. Only activate it when you need a fast stun break or you die.<br>
    <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="27014"></span>: keep it activated for group swiftness out of combat and just drop it when u can on the enemy group for a good aoe damage.<br>
    <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="26644"></span>: keep it activated for might stack out of combat and activate it if you can land it on enemy for extra damage or if you need energy to cast spells.<br>
    <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="27220"></span>: just activate it when you are being hammered with damage and you have no team support for heal reverse u can keep it activated instead of facet of strength in hard fights if you think you won’t have enough time for activation.<br>
    <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="27760"></span>: mostly being used in fights for super speed buff for allies in front of you and a really good CC.
    <h4>DWARF STANCE</h4>
    <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="26557"></span>: use it for cleaving downed enemies in melee range u will also take 20% less damage.<br>
    <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="28516"></span>: it’s a good source of stability for subgroup you can use it in fight.<br>
    <span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="27975"></span>: this one is your most important skill in this stance ignoring 50% of enemy physical damage incoming to you and your subgroup make sure you will use it in hard push.<br>
    Also use your true nature skill while on dwarf stance for 2 stacks of stability on allies can help.
<h4>HAMMER</h4>
<span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="28110"></span>: a good CC and range damage you can cast it and start moving back and make sure you use it in ally bobble.<br>
<span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="27976"></span>: commanders call it Phase it’s the best ranged spike in a blob when all revenants in squad use it on commander call it can down few enemies.<br>
<span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="28253"></span>: it’s also a good ranged spike with small cool down use it when it’s ready to use.<br>
<span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="28549"></span>: your auto attack don’t underestimate it, u can do a lot of damage to enemy only with this one keep auto attacking while your other hammer skills are on cool.
<h4>STAFF</h4>
<span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="28978"></span>: a really handy CC while you are in melee range of enemy also you evade attacks while using this.<br>
<span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29321"></span>: when you getting no cleanse and you got cripple, chilled or slow on you use this for cleanse<br>
<span data-armory-embed="skills" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="29288"></span>: use it as block when you are taking lot of damage or need to get to your tag.<br>
<h4>NOTES</h4>
<ol>
<li>Activate your facet of darkness, elements and strength out of combat. Consume facet of darkness ONLY fast emergency stun break.</li>
<li>start range damage on enemy with hammer 2-3, hammer 5 in ally bobble.</li>
<li>pop your facet of elements and strength to deal damage to enemy.</li>
<li>make sure in big push you do pop your dwarf stance ultimate.</li>
<li>CC the enemy with staff 5, and activate vengeful hammers in melee range.</li>
<li>staff 4 and hammer 5 for combo blasts.</li>
</ol>
<h4>BUILD VARIANTS</h4>
<span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="1781"></span> instead of <span data-armory-embed="traits" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="1774"></span>.<br>
<span data-armory-embed="items" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="24723"></span> instead of <span data-armory-embed="items" data-armory-size="20" data-armory-inline-text="wiki" data-armory-ids="24836"></span> if on low budget.<br>
Full berserker instead of marauder gear you will get bit low health but better for more damage.<br>
Retribution 232, instead of devastation trait line for more group support.
    ',
  ),
);

?>
